You grew up on a farm and became quite agile from
 continually rounding up wandering sheep. One Merchants
 Festival, while visiting the village with your family, you
 were intrigued with a group of wandering acrobats and decided
 that you wanted to become an acrobat yourself. At home you
 started practicing even though your parents complained that
 you were neglecting your duties. Finally, when you were just
 old enough to be called an adult, you ran away from home.
 
 You never did find that carnival troupe. You wandered to a small
 village deep in the forests and learned to fight with an axe
 and to pick locks and
 you began to train your mind to hold great knowledge.
 You know that your skills and your philosophy are what
 kept you alive and sane during the dark years when Jagar Tharn
 usurped the throne of the rightful emperor. Tharn's agents
 even searched out that little village, looking for political
 dissendents and other undesirables. Those they found
 disappeared forever.
 
 For the benefit of all, an uneasy alliance formed between you
 and all the other villagers who were in questionable lines of
 work. One night, you witnessed a ragged boy being pursued by
 a dozen blackclad agents of Jagar Tharn. You took a shortcut
 through the alleys to meet the boy, and it took little effort
 to convince him to run with you. The two of you stole into a
 concealed cellar and waited for the agents to pass. The boy
 was obviously not from the village, and when you asked him
 his name, he told you, hesistantly, "Turfar Enakbruk."
 You laughed at his royal sounding name, and he told you very
 seriously that he was the son of the rightful ruler of Tamriel
 and that an imposter was on the throne. At the time, you
 humored his boyish imagination. Now, of course, you know that
 he was telling the truth. You told him the best way out of the
 village, avoiding the areas frequented by the blackclad ones,
 and he told you that when he saw his father again, you would
 be properly thanked for that act of kindness.
 
 Some years later, your luck ran out and you were imprisoned
 on the charge of pickpocketing around the village.
 You had been in jail for several days when you received a
 visitor in the full regalia of an Imperial courier. He told
 you that in reward for your service to Prince Ghugur,
 you were granted full pardon and a bag filled with gold
 You were also given a simple, unpretentious letter inviting
 you the Imperial City for an audience with the Emperor. You
 had your eyes set on a bigger fishpond anyhow, so you went.
